/**
* knx.js - a pure Javascript library for KNX
* (C) 2016 Elias Karakoulakis
*/

//
// DPT12.*:  4-byte unsigned value
//

exports.formatAPDU = function(value) {
  if (!value || typeof value != 'number')
    throw 'Must supply a number for DPT12';
  var apdu_data = new Buffer(4);
  apdu_data.writeUInt32BE(value,0);
  return apdu_data;
}

exports.fromBuffer = function(buf) {
  if (buf.length != 4) throw "Buffer should be 4 bytes long";
  return buf.readUInt32BE(0);
}

// DPT12 base type info
exports.basetype = {
  "bitlength" : 32,
  "valuetype" : "basic",
  "desc" : "4-byte unsigned value",
  "range" : [0, Math.pow(2, 32)-1]
}

// DPT12 subtype info
exports.subtypes = {
  // 12.001 counter pulses
  "001" : {
    "name" : "DPT_Value_4_Ucount", "desc" : "counter pulses"
  }
}
